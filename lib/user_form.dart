import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class UserForm extends StatefulWidget {
  String userId;
  UserForm({Key? key, required this.userId}) : super(key: key);

  @override
  _UserFormState createState() => _UserFormState(this.userId);
}

class _UserFormState extends State<UserForm> {
  String userId;
  String fullName = "";
  String company = "";
  String age = "0";
  CollectionReference users = FirebaseFirestore.instance.collection('users');
  _UserFormState(this.userId);
  TextEditingController _fullNameController = new TextEditingController();
  TextEditingController _companyController = new TextEditingController();
  TextEditingController _ageController = new TextEditingController();
  @override
  void initState() {
    super.initState();
    if (this.userId.isNotEmpty) {
      users.doc(this.userId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data() as Map<String, dynamic>;
          fullName = data['full_name'];
          company = data['company'];
          age = data['age'].toString();
          _fullNameController.text = fullName;
          _companyController.text = company;
          _ageController.text = age;
        }
      });
    }
  }

  final _formkey = GlobalKey<FormState>();

  Future<void> addUser() {
    return users
        .add({
          'full_name': this.fullName,
          'company': this.company,
          'age': int.parse(age)
        })
        .then((value) => print('User Added'))
        .catchError((error) => print('Failed to add user: $error'));
  }

  Future<void> updateUser() {
    return users
        .doc(this.userId)
        .update({
          'company': company,
          'full_name': this.fullName,
          'age': int.parse(age)
        })
        .then((value) => print("User Updated"))
        .catchError((error) => print("Failed to update user: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('User'),
      ),
      body: Form(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        key: _formkey,
        child: Column(
          children: [
            TextFormField(
              controller: _fullNameController,
              decoration: InputDecoration(labelText: 'Full Name'),
              onChanged: (value) {
                setState(() {
                  fullName = value;
                });
              },
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please input Name';
                }
                return null;
              },
            ),
            TextFormField(
              controller: _companyController,
              decoration: InputDecoration(labelText: 'Company'),
              onChanged: (value) {
                setState(() {
                  company = value;
                });
              },
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please input Company';
                }
                return null;
              },
            ),
            TextFormField(
              initialValue: age,
              decoration: InputDecoration(labelText: 'Age'),
              onChanged: (value) {
                setState(() {
                  age = value;
                });
              },
              validator: (value) {
                if (value == null ||
                    value.isEmpty ||
                    int.tryParse(value) == null) {
                  return 'Please input Age';
                }
                return null;
              },
            ),
            ElevatedButton(
                onPressed: () async {
                  if (_formkey.currentState!.validate()) {
                    if (userId.isEmpty) {
                      await addUser();
                    } else {
                      await updateUser();
                    }
                    Navigator.pop(context);
                  }
                },
                child: Text('Save'))
          ],
        ),
      ),
    );
  }
}
