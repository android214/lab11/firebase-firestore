import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:example_project/user_form.dart';

class UserInformation extends StatefulWidget {
  UserInformation({Key? key}) : super(key: key);

  @override
  _UserInformationState createState() => _UserInformationState();
}

class _UserInformationState extends State<UserInformation> {
  final Stream<QuerySnapshot> _userStream = FirebaseFirestore.instance
      .collection('users')
      .where('age', isLessThan: 22)
      .snapshots();
  CollectionReference users = FirebaseFirestore.instance
      .collection('users');
  Future<void> delUser(userId) {
    return users
    .doc(userId)
    .delete()
    .then((value) => print('User Delete'))
    .catchError((error) => print('Failed to delete user: $error'));
  }
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _userStream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text("Loading");
        }

        return ListView(
          children: snapshot.data!.docs.map((DocumentSnapshot document) {
            Map<String, dynamic> data = document.data()! as Map<String, dynamic>;
            return ListTile(
              title: Text(data['full_name']),
              subtitle: Text(data['company']),
              onTap: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) => UserForm(userId: document.id)));
              },
              trailing: IconButton(
                icon: Icon(Icons.delete),
                onPressed: () async {
                  await delUser(document.id);
                },
              ),
            );
          }).toList(),
        );
      }
    );
  }
}